import { BotCommand } from "./command";
import { logger } from "./config";
import { SlackAPI } from "../slack_interface/slack_api";
import { SlackChannel } from "../slack_interface/channel";
import { SlackUser } from "../slack_interface/user";
import { TSMap } from "typescript-map";
import { MessageEvent } from "../slack_interface/rtm_types";
import {
    BotReply,
    isResponse,
    isReact,
    assertNever,
    Timestamp,
    isInvisibleError,
    BotError,
    isVisibleError,
} from "./botTypes";

export class Bot {
    private COMMAND_BEGIN = "!";
    private commands: TSMap<string, BotCommand>;
    private slackAPI: SlackAPI;

    constructor(slackAPI: SlackAPI) {
        this.slackAPI = slackAPI;
        this.commands = new TSMap();
    }

    public isCommand(msg: string): boolean {
        return msg.startsWith(this.COMMAND_BEGIN);
    }

    get registered_commands(): TSMap<string, BotCommand> {
        return this.commands.clone();
    }

    public registerCommand(command: BotCommand): void {
        this.commands.set(command.name, command);
    }

    public alias(oldName: string, newName: string): void {
        const cmd = this.commands.get(oldName);
        if(cmd == undefined) {
            logger.warn(`Trying to alias non-existing command ${newName}->${oldName}`);
            return;
        }
        this.commands.set(newName, cmd);
    }

    /**
     * Check incoming messages for commands of the form:
     * `!COMMAND_NAME ARGS*`
     */
    public async parseEvent(event: MessageEvent): Promise<void> {
        const res = (await this.slackAPI.getUserById(event.user)).map(
            async (user) => {
                (await this.slackAPI.getChannelById(event.channel)).map(
                    (channel) => {
                        const message = event.text;
                        this.processMessage(
                            message,
                            event.event_ts,
                            user,
                            channel
                        );
                    }
                );
            }
        );

        if (res.isErr()) {
            const error = res.error;
            logger.error("Parsing an event returned error", error);
        }
    }

    private processMessage(
        message: string,
        messageTimestamp: Timestamp,
        user: SlackUser,
        channel: SlackChannel
    ): void {
        if (!this.isCommand(message)) {
            return;
        }

        logger.info(
            `Handling command string: '${message}' from user ${user.real_name}(${user.id})`
        );
        const [command, ...args] = message.slice(1).split(" ");

        if (this.commands.has(command)) {
            this.handleCommand(
                command,
                channel,
                messageTimestamp,
                user,
                args
            ).catch((e) => {
                this.slackAPI.postMessageToChannel(
                    `Exception running command \`${command}\`.`,
                    channel
                );
                logger.error(
                    `${command}(msg: ${message}) threw an exception!`,
                    e
                );
            });
        } else {
            this.slackAPI.postMessageToChannel(
                `Command: \`${command}\` not found!`,
                channel
            );
        }
    }

    private async handleCommand(
        commandName: string,
        channel: SlackChannel,
        timestamp: Timestamp,
        user: SlackUser,
        args: string[]
    ): Promise<void> {
        const command = this.commands.get(commandName);
        const result = await command.callback(
            this.slackAPI,
            command,
            this,
            channel,
            timestamp,
            user,
            args
        );

        logger.info(`Handled command "${command.name}"`);

        if (result.isOk()) {
            const reply: BotReply = result.value;

            if (reply == null) {
                return;
            } else if (isResponse(reply)) {
                this.slackAPI.postMessageToChannel(reply.responseMsg, channel);
            } else if (isReact(reply)) {
                void this.slackAPI.reactToMessage(
                    channel,
                    timestamp,
                    reply.react
                );
            } else {
                //Should be exhaustive
                return assertNever(reply);
            }
        } else {
            const reply: BotError = result.error;
            logger.info(`Error running command: ${command.name}`);

            if (isInvisibleError(reply)) {
                // TODO: Post to admin?
                logger.error(reply.loggerMsg, reply.hiddenError);
                this.slackAPI.postMessageToChannel(
                    `:x: Something went wrong running your command ${command.name}!`,
                    channel
                );
            } else if (isVisibleError(reply)) {
                this.slackAPI.postMessageToChannel(
                    `:x: ${reply.visibleMsg}`,
                    channel
                );
            } else {
                //Should be exhaustive
                return assertNever(reply);
            }
        }
    }
}
