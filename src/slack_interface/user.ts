import { User } from "./rtm_types";

export class SlackUser implements User {
    private userInfo: User;

    constructor(userInfo: User) {
        this.userInfo = userInfo;
    }

    get id(): string {
        return this.userInfo.id;
    }

    get team_id(): string {
        return this.userInfo.team_id;
    }

    get name(): string {
        return this.userInfo.name;
    }

    get deleted(): boolean {
        return this.userInfo.deleted;
    }

    get color(): string {
        return this.userInfo.color;
    }

    get real_name(): string {
        return this.userInfo.real_name;
    }

    get is_admin(): boolean {
        return this.userInfo.is_admin;
    }

    get is_owner(): boolean {
        return this.userInfo.is_owner;
    }

    get is_primary_owner(): boolean {
        return this.userInfo.is_primary_owner;
    }

    get is_restricted(): boolean {
        return this.userInfo.is_restricted;
    }

    get is_ultra_restricted(): boolean {
        return this.userInfo.is_ultra_restricted;
    }

    get is_bot(): boolean {
        return this.userInfo.is_bot;
    }

    get is_app_user(): boolean {
        return this.userInfo.is_app_user;
    }

    get updated(): number {
        return this.userInfo.updated;
    }

    public getUsername(): string {
        return this.userInfo.name;
    }
}
