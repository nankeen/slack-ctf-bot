import { WebClient } from "@slack/web-api";
import { User, UserListing } from "./rtm_types";
import { Result, ok, err } from "neverthrow";
import { CachedValueStore } from "../core/cached_value";
import { SlackUser } from "./user";

export class SlackUserManager {
    private static USER_CACHE_LIMIT = 100;

    private users: CachedValueStore<Map<string, SlackUser>>;
    private web: WebClient;

    constructor(web: WebClient) {
        this.users = new CachedValueStore(
            SlackUserManager.USER_CACHE_LIMIT,
            "User Cache",
            () => this.getUsers()
        );

        this.web = web;
    }

    public async getUserById(id: string): Promise<Result<SlackUser, Error>> {
        return (await this.users.getCachedValue()).andThen((users) => {
            const user = users.get(id);

            if (user) {
                return ok(user);
            } else {
                return err(Error(`Couldn't find user with id: ${id}`));
            }
        });
    }

    public async getUsers(): Promise<Result<Map<string, SlackUser>, Error>> {
        try {
            const userListing = (await this.web.users.list()) as UserListing;

            return ok(
                new Map(
                    userListing.members.map((user: User) => {
                        return [user.id, new SlackUser(user)];
                    })
                )
            );
        } catch (e) {
            return err(e);
        }
    }
}
