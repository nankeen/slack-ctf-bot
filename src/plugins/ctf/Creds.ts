import { CredsProp } from "@prisma/client";
import { CTFDB } from "./db/CTFDB";
import { CTF } from "./CTF";
import { Result } from "neverthrow";

export class Creds implements CredsProp {
    props: CredsProp;

    private constructor(props: CredsProp) {
        this.props = props;
    }

    get rowid(): number {
        return this.props.rowid;
    }

    get username(): string {
        return this.props.username;
    }
    get password(): string {
        return this.props.password;
    }
    get ctfId(): number {
        return this.props.ctfId;
    }

    public static async getCredsForCtf(
        db: CTFDB,
        ctf: CTF
    ): Promise<Result<Creds, Error>> {
        const props = await db.getCredsPropForCtf(ctf);

        return props.map((prop) => {
            return new this(prop);
        });
    }

    public static async setCredsForCtf(
        db: CTFDB,
        ctf: CTF,
        username: string,
        password: string
    ): Promise<Result<Creds, Error>> {
        const props = await db.setCredsPropForCtf(ctf, username, password);

        return props.map((prop) => {
            return new this(prop);
        });
    }
}
