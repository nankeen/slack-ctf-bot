import { err, Result, ok } from "neverthrow";
import { visErr, BotError } from "../../core/botTypes";
import { SlackAPI } from "../../slack_interface/slack_api";
import { CTF } from "./CTF";
import { Challenge } from "./Challenge";

export async function doAddChallenge(
    api: SlackAPI,
    currentCtf: CTF,
    challengeName: string,
    challengeType: string
): Promise<Result<Challenge, BotError>> {
    const channelName = `${currentCtf.name}-${challengeName}`;

    const createChannelOrErr = await api.createChannel(
        channelName,
        true //private
    );

    if (createChannelOrErr.isErr()) {
        return err(
            visErr(`Failed to create challenge channel: ${channelName}`)
        );
    }

    const channel = createChannelOrErr.value;

    const addedChallengeOrErr = await currentCtf.addChallenge(
        challengeName,
        challengeType,
        channel
    );

    if (addedChallengeOrErr.isErr()) {
        return err(visErr(`Failed to create challenge: ${challengeName}`));
    }

    return ok(addedChallengeOrErr.value);
}
