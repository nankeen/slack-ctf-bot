import { CTFDB } from "./db/CTFDB";
import { ChannelId } from "../../slack_interface/rtm_types";
import { ChallengeProps } from "@prisma/client";
import { ChallengeWorker } from "./ChallengeWorker";
import { Result } from "neverthrow";
import { CTF } from "./CTF";
import { SlackChannel } from "../../slack_interface/channel";
import { SlackUser } from "../../slack_interface/user";

type ChallengeId = number;

export class Challenge {
    private db: CTFDB;
    private props: ChallengeProps;

    private constructor(db: CTFDB, props: ChallengeProps) {
        this.db = db;
        this.props = props;
    }

    public static async getChallenge(
        db: CTFDB,
        ctf: CTF,
        name: string
    ): Promise<Result<Challenge, Error>> {
        const propsOrErr = await db.getCtfChallengeByName(ctf, name);

        return propsOrErr.map((props) => {
            return new Challenge(db, props);
        });
    }

    public static async getChallenges(
        db: CTFDB,
        ctf: CTF
    ): Promise<Array<Challenge>> {
        const challengeProps = await db.getChallengeProps(ctf);

        return challengeProps.map((prop) => new this(db, prop));
    }

    public static async addChallenge(
        db: CTFDB,
        name: string,
        challengeType: string,
        ctf: CTF,
        channel: SlackChannel
    ): Promise<Result<Challenge, Error>> {
        return (
            await db.newChallengeProps(name, challengeType, ctf, channel)
        ).map((props) => {
            return new this(db, props);
        });
    }

    public static async getChallengeFromChannel(
        db: CTFDB,
        channel: SlackChannel
    ): Promise<Result<Challenge, Error>> {
        return (await db.getChallengePropsFromChannel(channel)).map((props) => {
            return new this(db, props);
        });
    }

    get rowid(): ChallengeId {
        return this.props.rowid;
    }

    public async isSolved(): Promise<boolean> {
        const workers = await this.getWorkers();
        return workers.some((worker) => worker.isSolved);
    }

    public async addWorker(
        user: SlackUser
    ): Promise<Result<ChallengeWorker, Error>> {
        return ChallengeWorker.addWorkerToChallenge(this.db, this, user);
    }

    get name(): string {
        return this.props.name;
    }

    get type(): string {
        return this.props.type;
    }

    public async markSolved(users: Array<SlackUser>): Promise<void> {
        await this.db.markSolved(this, users);
    }

    public async markUnsolved(): Promise<void> {
        await this.db.markUnsolved(this);
    }

    public getWorkers(): Promise<Array<ChallengeWorker>> {
        return ChallengeWorker.workersOnChallenge(this.db, this);
    }

    public async solvers(): Promise<Array<ChallengeWorker>> {
        const workers = await this.getWorkers();

        return workers.filter((worker: ChallengeWorker) => worker.solved);
    }

    get channel(): ChannelId {
        return this.props.channelId;
    }
}
