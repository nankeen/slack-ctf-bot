import { Bot } from "../../core/bot";
import { CommandFunction } from "../../core/command";
import { SlackChannel } from "../../slack_interface/channel";
import { ok } from "neverthrow";
import { resp } from "../../core/botTypes";

//TODO: Create a plugin interface
export class DefaultCommands {
    bot: Bot;

    constructor(bot: Bot) {
        this.bot = bot;
    }

    /**
     * Called when this plugin should be initialised and registers callbacks and any other setup
     */
    //TODO: Add some middleware that can add the qualifiers like "in a channel|as an admin" whilst checking those constraints
    //TODO: Arg parsing auto docs
    public init = (bot: Bot): void => {
        bot.registerCommand({
            name: "help",
            callback: this.help,
            description: "Get the help info for the bot",
            usage: "!help",
        });

        bot.registerCommand({
            name: "ping",
            callback: this.ping,
            description: "Check the bot is alive",
            usage: "!ping",
        });
    };


    public ping: CommandFunction = (
        _api,
        _command,
        _bot,
        _channel: SlackChannel,
        _timestamp,
        _user,
        _args
    ) => {
        return ok(resp("Pong!"));
    };

    public help: CommandFunction = (
        _api,
        _command,
        _bot,
        _channel: SlackChannel,
        _timestamp,
        _user,
        _args
    ) => {
        return ok(
            resp(
                this.bot.registered_commands
                    .map((cmd, key) => {
                        if (key){
                            return `\`!${key}\`\n    (${cmd.description})`;
                        }else {
                            return `\`${cmd.usage}\`\n    (${cmd.description})`;
                        }
                    })
                    .join("\n")
            )
        );
    };
}
